$('#newsletterSubsctibeInput').on('input', function (event) {
    if ($(this)[0]['validity']['valid'] === false) {
        if (!($(this).hasClass('is-invalid'))) {
            $(this).addClass('is-invalid');
            $(this).offsetParent().addClass('is-invalid');
            $('#subscribe-btn').attr('disabled', true)
        }
        if (event.target.value === '' && $(this).hasClass('is-invalid')) {
            $(this).removeClass('is-invalid');
            $(this).offsetParent().removeClass('is-invalid');
        }
    } else if ($(this)[0]['validity']['valid'] === true && $(this).hasClass('is-invalid')) {
        $(this).removeClass('is-invalid');
        $(this).offsetParent().removeClass('is-invalid');
        $('#subscribe-btn').removeAttr('disabled')
    }
});

function newsletterSubsctibe()
{
    let header = body = ''
    let svg_ok = `
        <svg
            xmlns="http://www.w3.org/2000/svg"
            height="1.1em"
            viewBox="0 0 512 512"
            class="text-success"
            fill="currentColor"
        >
            <path
                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z"
            />
        </svg>
    `
    let svg_er = `
        <svg
            xmlns="http://www.w3.org/2000/svg"
            height="1.1em"
            viewBox="0 0 512 512"
            class="text-danger"
            fill="currentColor"
        >
            <path
                d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zm0-384c13.3 0 24 10.7 24 24V264c0 13.3-10.7 24-24 24s-24-10.7-24-24V152c0-13.3 10.7-24 24-24zM224 352a32 32 0 1 1 64 0 32 32 0 1 1 -64 0z"
            />
        </svg>
    `

    let header_ok = 'We are incredibly happy'
    let header_er = 'An error has occurred'

    let data_ok = 'Thank you for subscribing to our newsletter. We\'ll soon start sending you the latest news, unique offers, events and discounts. Check your email as often as possible to stay up to date with all our company\'s offers'
    let data_er1 = 'You probably made a mistake and entered an incorrect email. Check the email and try again'
    let data_er2 = 'You have previously subscribed. Thank you for your interest.'

    // $.ajax({
    //     url: url + 'newsletter',
    //     method: 'POST',
    //     dataType: 'json',
    //     username: username,
    //     password: password,
    //     data: {'email': $('#subscribeInput').val()},
    //     success: function(data){
    //         console.log(data);
    //     }
    // });
    
// OK
    header = `
    <div class="border-start border-5 border-primary ps-2 fw-bold text-muted text-uppercase user-select-none mb-3">
        <div class="d-flex align-items-center font-roboto">
            <div>
                ` + svg_ok + `
            </div>
            <div class="ps-1 font-roboto text-break">
                ` + header_ok + `
            </div>
        </div>
    </div>
    `
    body = data_ok
// Error Invalid Email
    header = `
        <div class="border-start border-5 border-primary ps-2 fw-bold text-muted text-uppercase user-select-none mb-3">
            <div class="d-flex align-items-center font-roboto">
                <div>
                    ` + svg_er + `
                </div>
                <div class="ps-1 font-roboto text-break">
                    ` + header_er + `
                </div>
            </div>
        </div>
    `
    body = data_er1
// Error Subscribed!
    header = `
    <div class="border-start border-5 border-primary ps-2 fw-bold text-muted text-uppercase user-select-none mb-3">
        <div class="d-flex align-items-center font-roboto">
            <div>
                ` + svg_er + `
            </div>
            <div class="ps-1 font-roboto text-break">
                ` + header_er + `
            </div>
        </div>
    </div>
    `
    body = data_er2

    $('#newsletterSubscribeModalLabel').html(header)
    $('#newsletterSubscribeModalBody').html(body)
    $('#newsletterSubscribeModal').modal('show');
}
