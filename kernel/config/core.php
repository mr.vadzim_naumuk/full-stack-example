<?php
if (preg_match('/^devel\d+./', $_SERVER['HTTP_HOST'], $devel))
    require_once '../kernel/lib/Devel.php';
define('___DEVEL_PREFIX___', (boolval(!empty($devel[0]))) ? $devel[0] : null);
unset($devel);

error_reporting(-1);
ini_set('display_errors', boolval(!empty(___DEVEL_PREFIX___)));
ini_set('display_startup_errors', boolval(!empty(___DEVEL_PREFIX___)));
ini_set('html_errors', boolval(!empty(___DEVEL_PREFIX___)));

date_default_timezone_set('UTC');
ini_set('default_charset', 'UTF-8');

spl_autoload_register(function(string $class)
{
    if (file_exists($class = str_replace('\\', '/', "../{$class}.php")))
        require_once $class;
    unset($class);
});
