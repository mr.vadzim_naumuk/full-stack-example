<?php
namespace kernel\core;
use kernel\core\View;

Class Response
{
    private int $code = 200;
    private ?string $type = null;
    private ?string $response = null;

    public function __construct()
    {
        Router::parse_route();
        if (is_numeric(Router::$route))
            $this->code = Router::$route;
        $this->response = View::render_page(Router::$route);
        $this->type = View::$content_type;
    }

    public function __destruct()
    {
        http_response_code($this->code);
        header("Content-type: {$this->type}; charset=".ini_get('default_charset'));
        die($this->response);
        // unset($this->code, $this->type, $this->response);
    }
}
