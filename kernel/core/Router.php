<?php
namespace kernel\core;

class Router
{
    public static ?string $route = null;

    public static function parse_route()
    {
        $route = trim(parse_url($_SERVER['REQUEST_URI'])['path'], '/');
        if (empty($route))
            $route = 'index';
        self::$route = (!file_exists("../source/backend/{$route}.php") && !file_exists("../source/frontend/layer/{$route}.html")) ? '404' : $route;
        unset($route);
    }
}
