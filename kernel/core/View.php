<?php
namespace kernel\core;

Class View
{
    private static array $vars = [];
    private static array $modal = [];
    public static string $content_type = 'text/html';

    public static final function set_vars($key, $value)
    {
        self::$vars[$key] = $value;
        unset($key, $value);
    }

    public static final function set_modal($window)
    {
        self::$modal[] = $window;
    }

    public static final function set_default_modals()
    {
        self::set_modal('locale_settings');
        self::set_modal('newsletter');
    }

    public static final function render_page($route): string
    {
        self::set_default_modals();

        $meta = [
            'title' => 'Cheap Travel',
            'description' => '',
            'keywords' => ''
        ];

        if (file_exists("../source/backend/{$route}.php"))
            require_once "../source/backend/{$route}.php";
        if (!empty(self::$vars))
            extract(self::$vars);

        ob_start();
        {
            require_once('../source/frontend/template/header.html');
    
            require_once('../source/frontend/block/preloader.html');
            require_once('../source/frontend/block/noscript.html');

            require_once('../source/frontend/block/header.html');

            if (file_exists("../source/frontend/layer/{$route}.html"))
                require_once("../source/frontend/layer/{$route}.html");

            require_once('../source/frontend/block/footer.html');

            if (!empty(self::$modal)) {
                foreach (self::$modal as $value)
                    require_once("../source/frontend/modal/{$value}.html");
            }

            require_once('../source/frontend/template/footer.html');
        }
        return ob_get_clean();
    }
}
