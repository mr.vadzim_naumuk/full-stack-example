<?php
function _d($value, bool $exit = true, bool $vd = false)
{
    header('Content-type: text/html; charset='.ini_get('default_charset'));
    echo '<pre>';
    ($vd) ? var_dump($value) : print_r($value);
    echo '<pre>';
    if ($exit)
        exit(0);
    unset($value, $exit, $vd);
}

$_SERVER['HTTP_HOST'] = preg_replace("/^{$devel[0]}/", null, $_SERVER['HTTP_HOST']);
