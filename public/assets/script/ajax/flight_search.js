async function search(rq)
{    
    render_result(await search_flight(['' + rq]))
}

async function search_flight(urls)
{
    try {
        let response = await Promise.all(
            [urls].map(
                url =>
                    fetch(url).then(
                        (response) => response.json()
                    )
                )
            )
        return response
    } catch (error) {
        console.log(error)
        throw(error)
    }
}

function render_result(data)
{
    let results = []
    let result = ''

    if (!data)
        result = `
            <div class="row mb-3">
                <div class="col p-3 shadow rounded bg-white">
                    Sorry, we could not find flights on the specified route.
                </div>
            </div>
        `
    else {
        data.map(function (flights) {
            if (flights.length > 0) {
                flights.map(function (flight) {
                    results.push(flight)
                });
            }
        });
        console.log(results)
    }
    $('#flight-result').html(result)
}
