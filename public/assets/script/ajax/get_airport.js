function get_airport(rq, id)
{
    $.ajax({
        url: '',
        method: 'GET',
        dataType: 'json',
        data: { 'q': rq, hl: 'en' },
        success: function(data) {
            if (data != '') {
                $(document).off('click')
                $('#airports-' + id).html(render_airport_list(data, id))
                if ($('#list-' + id).hasClass('d-none'))
                    $('#list-' + id).removeClass('d-none')

                $(document).on('click', function(click) {
                    if ($(click.target).offsetParent().attr('id') !== 'list-' + id) {
                        set_airport('',id)
                    }
                })
            } else {
                if (!$('#list-' + id).hasClass('d-none')) {
                    $('#list-' + id).addClass('d-none')
                }
            }
        }
    })
}
