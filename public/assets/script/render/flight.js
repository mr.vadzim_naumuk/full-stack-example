const oneway = `
    <div>
        <svg
            xmlns="http://www.w3.org/2000/svg"
            height="1em"
            viewBox="0 0 512 512"
            fill="currentColor"
        >
            <path
                d="M334.5 414c8.8 3.8 19 2 26-4.6l144-136c4.8-4.5 7.5-10.8 7.5-17.4s-2.7-12.9-7.5-17.4l-144-136c-7-6.6-17.2-8.4-26-4.6s-14.5 12.5-14.5 22l0 72L32 192c-17.7 0-32 14.3-32 32l0 64c0 17.7 14.3 32 32 32l288 0 0 72c0 9.6 5.7 18.2 14.5 22z"
            />
        </svg>
    </div>
    <div class="ps-1 font-roboto text-break">Oneway</div>
`
const round = `
    <div>
        <svg
            xmlns="http://www.w3.org/2000/svg"
            height="1em"
            viewBox="0 0 512 512"
            fill="currentColor"
        >
            <path
                d="M0 224c0 17.7 14.3 32 32 32s32-14.3 32-32c0-53 43-96 96-96H320v32c0 12.9 7.8 24.6 19.8 29.6s25.7 2.2 34.9-6.9l64-64c12.5-12.5 12.5-32.8 0-45.3l-64-64c-9.2-9.2-22.9-11.9-34.9-6.9S320 19.1 320 32V64H160C71.6 64 0 135.6 0 224zm512 64c0-17.7-14.3-32-32-32s-32 14.3-32 32c0 53-43 96-96 96H192V352c0-12.9-7.8-24.6-19.8-29.6s-25.7-2.2-34.9 6.9l-64 64c-12.5 12.5-12.5 32.8 0 45.3l64 64c9.2 9.2 22.9 11.9 34.9 6.9s19.8-16.6 19.8-29.6V448H352c88.4 0 160-71.6 160-160z"
            />
        </svg>
    </div>
    <div class="ps-1 font-roboto text-break">Roundtrip</div>
`
const multi = `
    <div>
        <svg
            xmlns="http://www.w3.org/2000/svg"
            height="1em"
            viewBox="0 0 512 512"
            fill="currentColor"
        >
            <path
                d="M352 256c0 22.2-1.2 43.6-3.3 64H163.3c-2.2-20.4-3.3-41.8-3.3-64s1.2-43.6 3.3-64H348.7c2.2 20.4 3.3 41.8 3.3 64zm28.8-64H503.9c5.3 20.5 8.1 41.9 8.1 64s-2.8 43.5-8.1 64H380.8c2.1-20.6 3.2-42 3.2-64s-1.1-43.4-3.2-64zm112.6-32H376.7c-10-63.9-29.8-117.4-55.3-151.6c78.3 20.7 142 77.5 171.9 151.6zm-149.1 0H167.7c6.1-36.4 15.5-68.6 27-94.7c10.5-23.6 22.2-40.7 33.5-51.5C239.4 3.2 248.7 0 256 0s16.6 3.2 27.8 13.8c11.3 10.8 23 27.9 33.5 51.5c11.6 26 20.9 58.2 27 94.7zm-209 0H18.6C48.6 85.9 112.2 29.1 190.6 8.4C165.1 42.6 145.3 96.1 135.3 160zM8.1 192H131.2c-2.1 20.6-3.2 42-3.2 64s1.1 43.4 3.2 64H8.1C2.8 299.5 0 278.1 0 256s2.8-43.5 8.1-64zM194.7 446.6c-11.6-26-20.9-58.2-27-94.6H344.3c-6.1 36.4-15.5 68.6-27 94.6c-10.5 23.6-22.2 40.7-33.5 51.5C272.6 508.8 263.3 512 256 512s-16.6-3.2-27.8-13.8c-11.3-10.8-23-27.9-33.5-51.5zM135.3 352c10 63.9 29.8 117.4 55.3 151.6C112.2 482.9 48.6 426.1 18.6 352H135.3zm358.1 0c-30 74.1-93.6 130.9-171.9 151.6c25.5-34.2 45.2-87.7 55.3-151.6H493.4z"
            />
        </svg>
    </div>
    <div class="ps-1 font-roboto text-break">Multicity</div>
`
const svg_city = `
    <svg
        xmlns="http://www.w3.org/2000/svg"
        height="1em"
        viewBox="0 0 640 512"
        fill="currentColor"
    >
        <path
            d="M480 48c0-26.5-21.5-48-48-48H336c-26.5 0-48 21.5-48 48V96H224V24c0-13.3-10.7-24-24-24s-24 10.7-24 24V96H112V24c0-13.3-10.7-24-24-24S64 10.7 64 24V96H48C21.5 96 0 117.5 0 144v96V464c0 26.5 21.5 48 48 48H304h32 96H592c26.5 0 48-21.5 48-48V240c0-26.5-21.5-48-48-48H480V48zm96 320v32c0 8.8-7.2 16-16 16H528c-8.8 0-16-7.2-16-16V368c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16zM240 416H208c-8.8 0-16-7.2-16-16V368c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v32c0 8.8-7.2 16-16 16zM128 400c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V368c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v32zM560 256c8.8 0 16 7.2 16 16v32c0 8.8-7.2 16-16 16H528c-8.8 0-16-7.2-16-16V272c0-8.8 7.2-16 16-16h32zM256 176v32c0 8.8-7.2 16-16 16H208c-8.8 0-16-7.2-16-16V176c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16zM112 160c8.8 0 16 7.2 16 16v32c0 8.8-7.2 16-16 16H80c-8.8 0-16-7.2-16-16V176c0-8.8 7.2-16 16-16h32zM256 304c0 8.8-7.2 16-16 16H208c-8.8 0-16-7.2-16-16V272c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v32zM112 320H80c-8.8 0-16-7.2-16-16V272c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v32c0 8.8-7.2 16-16 16zm304-48v32c0 8.8-7.2 16-16 16H368c-8.8 0-16-7.2-16-16V272c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16zM400 64c8.8 0 16 7.2 16 16v32c0 8.8-7.2 16-16 16H368c-8.8 0-16-7.2-16-16V80c0-8.8 7.2-16 16-16h32zm16 112v32c0 8.8-7.2 16-16 16H368c-8.8 0-16-7.2-16-16V176c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16z"
        />
    </svg>
`
const svg_airport = `
    <svg
        xmlns="http://www.w3.org/2000/svg"
        height="1em"
        viewBox="0 0 512 512"
        fill="currentColor"
    >
        <path
            d="M192 93.7C192 59.5 221 0 256 0c36 0 64 59.5 64 93.7l0 66.3L497.8 278.5c8.9 5.9 14.2 15.9 14.2 26.6v56.7c0 10.9-10.7 18.6-21.1 15.2L320 320v80l57.6 43.2c4 3 6.4 7.8 6.4 12.8v42c0 7.8-6.3 14-14 14c-1.3 0-2.6-.2-3.9-.5L256 480 145.9 511.5c-1.3 .4-2.6 .5-3.9 .5c-7.8 0-14-6.3-14-14V456c0-5 2.4-9.8 6.4-12.8L192 400V320L21.1 377C10.7 380.4 0 372.7 0 361.8V305.1c0-10.7 5.3-20.7 14.2-26.6L192 160V93.7z"
        />
    </svg>
`

function set_airport(value, id)
{
    $(document).off('click')
    $('#' + id).val(value)
    $('#list-' + id).addClass('d-none')
    $('#airports-' + id).html()
}


function delete_segment(segment)
{
    $('#segment-' + segment).remove()
}


function render_delete_btn(segment)
{
    return `
        <button id="segment-` + segment + `-delete" type="button" class="btn btn-outline-danger px-3" title="Delete" onclick="delete_segment(` + segment + `)">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                height="1em"
                viewBox="0 0 640 512"
                fill="currentColor"
            >
                <path
                    d="M256 0c-35 0-64 59.5-64 93.7v84.6L8.1 283.4c-5 2.8-8.1 8.2-8.1 13.9v65.5c0 10.6 10.2 18.3 20.4 15.4l171.6-49 0 70.9-57.6 43.2c-4 3-6.4 7.8-6.4 12.8v42c0 7.8 6.3 14 14 14c1.3 0 2.6-.2 3.9-.5L256 480l110.1 31.5c1.3 .4 2.6 .5 3.9 .5c6 0 11.1-3.7 13.1-9C344.5 470.7 320 422.2 320 368c0-60.6 30.6-114 77.1-145.6L320 178.3V93.7C320 59.5 292 0 256 0zM496 512a144 144 0 1 0 0-288 144 144 0 1 0 0 288zm59.3-180.7L518.6 368l36.7 36.7c6.2 6.2 6.2 16.4 0 22.6s-16.4 6.2-22.6 0L496 390.6l-36.7 36.7c-6.2 6.2-16.4 6.2-22.6 0s-6.2-16.4 0-22.6L473.4 368l-36.7-36.7c-6.2-6.2-6.2-16.4 0-22.6s16.4-6.2 22.6 0L496 345.4l36.7-36.7c6.2-6.2 16.4-6.2 22.6 0s6.2 16.4 0 22.6z"
                />
            </svg>
        </button>
    `
}

function render_airport_list(data, id)
{
    let result = ''
    for (let i = 0; i < data.length; i++)
        result += `
            <div class="col-12 p-0">
                <div class="row mb-3">
                    <button class="btn btn-outline-primary" type="button" onmousedown="set_airport('` + data[i]['value'] + ' ' + data[i]['name'] + ', ' + data[i]['cc'] + `','` + id + `')">
                        <div class="text-center">
                            <div class="d-flex align-items-center justify-content-center">
                                ` + eval('svg_' + data[i]['type']) + `
                                <span class="ps-2">` + data[i]['value'].toUpperCase() + `</span>
                            </div>
                            <div>` + data[i]['name'] + `, ` + data[i]['cc'] + `</div>
                        </div>
                    </button>
                </div>
            </div>
        `
    return result
}

function render_flight_input(segment)
{
    let departure = arrival = ''
    let delete_segment = (segment > 0) ? render_delete_btn(segment) : ''
    if (segment > 0) {
        departure = $('#arrival-' + (segment - 1)).val()
        if (segment === 1)
            arrival = $('#departure-' + (segment - 1)).val()
    }

    let new_segment = `
        <div id="segment-` + segment + `" class="row">
            <div class="col-12 mb-3">
                <div class="border-start border-5 border-primary ps-3 fw-bold text-uppercase text-primary user-select-none">
                    <div class="d-flex align-items-center font-roboto">
                        <div>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                height="1em"
                                viewBox="0 0 512 512"
                                fill="currentColor"
                            >
                                <path
                                    d="M192 93.7C192 59.5 221 0 256 0c36 0 64 59.5 64 93.7l0 66.3L497.8 278.5c8.9 5.9 14.2 15.9 14.2 26.6v56.7c0 10.9-10.7 18.6-21.1 15.2L320 320v80l57.6 43.2c4 3 6.4 7.8 6.4 12.8v42c0 7.8-6.3 14-14 14c-1.3 0-2.6-.2-3.9-.5L256 480 145.9 511.5c-1.3 .4-2.6 .5-3.9 .5c-7.8 0-14-6.3-14-14V456c0-5 2.4-9.8 6.4-12.8L192 400V320L21.1 377C10.7 380.4 0 372.7 0 361.8V305.1c0-10.7 5.3-20.7 14.2-26.6L192 160V93.7z"
                                />
                            </svg>
                        </div>
                        <div class="ps-1 font-roboto text-break">
                            Flight <span class="font-roboto" id="segment-` + segment + `-number">` + (segment + 1) + `</span>.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-3 position-relative">
                <div class="input-group has-validation text-secondary">
                    <button class="input-group-text btn btn-outline-primary px-3" type="button" onclick="$('#departure-` + segment + `').focus()">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            height="1em"
                            viewBox="0 0 640 512"
                            fill="currentColor"
                        >
                            <path
                                d="M381 114.9L186.1 41.8c-16.7-6.2-35.2-5.3-51.1 2.7L89.1 67.4C78 73 77.2 88.5 87.6 95.2l146.9 94.5L136 240 77.8 214.1c-8.7-3.9-18.8-3.7-27.3 .6L18.3 230.8c-9.3 4.7-11.8 16.8-5 24.7l73.1 85.3c6.1 7.1 15 11.2 24.3 11.2H248.4c5 0 9.9-1.2 14.3-3.4L535.6 212.2c46.5-23.3 82.5-63.3 100.8-112C645.9 75 627.2 48 600.2 48H542.8c-20.2 0-40.2 4.8-58.2 14L381 114.9zM0 480c0 17.7 14.3 32 32 32H608c17.7 0 32-14.3 32-32s-14.3-32-32-32H32c-17.7 0-32 14.3-32 32z"
                            />
                        </svg>
                    </button>
                    <div class="form-floating">
                        <input class="form-control rounded-end" type="text" oninput="get_airport($(this).val(), this.id)" onmouseup="$(this).val('')" placeholder="" required id="departure-` + segment + `" name="departure[` + segment + `]" value="` + departure + `" />
                        <label for="departure-` + segment + `" class="fst-italic fs-d9 text-wrap">
                            Departure
                            <span class="text-danger">*</span>
                        </label>
                    </div>
                    <div class="invalid-feedback text-start">
                        Please enter departure
                    </div>
                </div>
                <div id="list-departure-` + segment + `" class="position-absolute row m-0 w-100 pe-4 d-none" style="z-index:3">
                    <div class="col p-0 bg-white border rounded">
                        <div class="row m-3 mb-0" id="airports-departure-` + segment + `"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-3 position-relative">
                <div class="input-group has-validation text-secondary">
                    <button class="input-group-text btn btn-outline-primary px-3" type="button" onclick="$('#arrival-` + segment + `').focus()">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            height="1em"
                            viewBox="0 0 640 512"
                            fill="currentColor"
                        >
                            <path
                                d="M.3 166.9L0 68C0 57.7 9.5 50.1 19.5 52.3l35.6 7.9c10.6 2.3 19.2 9.9 23 20L96 128l127.3 37.6L181.8 20.4C178.9 10.2 186.6 0 197.2 0h40.1c11.6 0 22.2 6.2 27.9 16.3l109 193.8 107.2 31.7c15.9 4.7 30.8 12.5 43.7 22.8l34.4 27.6c24 19.2 18.1 57.3-10.7 68.2c-41.2 15.6-86.2 18.1-128.8 7L121.7 289.8c-11.1-2.9-21.2-8.7-29.3-16.9L9.5 189.4c-5.9-6-9.3-14-9.3-22.5zM32 448H608c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32zm96-80a32 32 0 1 1 64 0 32 32 0 1 1 -64 0zm128-16a32 32 0 1 1 0 64 32 32 0 1 1 0-64z"
                            />
                        </svg>
                    </button>
                    <div class="form-floating">
                        <input class="form-control rounded-end" type="text" oninput="get_airport($(this).val(), this.id)" onmouseup="$(this).val('')" placeholder="" required id="arrival-` + segment + `" name="arrival[` + segment + `]" value="` + arrival + `" />
                        <label for="arrival-` + segment + `" class="fst-italic fs-d9 text-wrap">
                            Arrival
                            <span class="text-danger">*</span>
                        </label>
                    </div>
                    <div class="invalid-feedback">
                        Please enter arrival
                    </div>
                </div>
                <div id="list-arrival-` + segment + `" class="position-absolute row m-0 w-100 pe-4 d-none" style="z-index:3">
                    <div class="col p-0 bg-white border rounded">
                        <div class="row m-3 mb-0" id="airports-arrival-` + segment + `"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-3">
                <div class="input-group has-validation text-secondary">
                    <button class="input-group-text btn btn-outline-primary px-3" type="button" onclick="$('#date-` + segment + `').focus()">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            height="1em"
                            width="20"
                            viewBox="0 0 448 512"
                            fill="currentColor"
                        >
                            <path
                                d="M128 0c17.7 0 32 14.3 32 32V64H288V32c0-17.7 14.3-32 32-32s32 14.3 32 32V64h48c26.5 0 48 21.5 48 48v48H0V112C0 85.5 21.5 64 48 64H96V32c0-17.7 14.3-32 32-32zM0 192H448V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V192zm64 80v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm128 0v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H208c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V272c0-8.8-7.2-16-16-16H336zM64 400v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H80c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H208zm112 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16V400c0-8.8-7.2-16-16-16H336c-8.8 0-16 7.2-16 16z"
                            />
                        </svg>
                    </button>
                    <div class="form-floating">
                        <input id="date-` + segment + `" class="form-control rounded-end" type="date" placeholder="" name="date[` + segment + `]" required/>
                        <label for="date-` + segment + `" class="fst-italic fs-d9 text-wrap">
                            Date
                            <span class="text-danger">*</span>
                        </label>
                    </div>
                    <div class="invalid-feedback">
                        Please enter date
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mb-3">
                <div class="h-100 row align-items-center">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class=col-sm-3>Direct flight</div>
                            <div class=col-sm-3>Add hotel</div>
                            <div class=col-sm-3>Add transfer</div>
                        </div>
                    </div>
                    <div class="col-lg-4 align-self-center text-end p-0">
                        ` + delete_segment + `
                    </div>
                </div>
            </div>
            <div class="col-12 px-3 mb-3 border-bottom border-secondary-subtle"></div>
        </div>
    `
    if (segment == 0) {
        $('#segments').prepend(new_segment)
        $('#flight-type').html(oneway)
    } else {
        $(new_segment).insertAfter('#segment-' + (segment - 1))
        $('#flight-type').html((segment == 1) ? round : multi)
    }

    $('#add_segment').attr('onclick', 'render_flight_input(' + (segment + 1) + ')')
}
