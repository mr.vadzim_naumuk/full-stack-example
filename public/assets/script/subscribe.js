$('#newsLetterSubscribe').on('input', function (event) {
    if ($(this)[0]['validity']['valid'] === false) {
        if (!($(this).hasClass('is-invalid'))) {
            $(this).addClass('is-invalid');
            $(this).offsetParent().addClass('is-invalid');
            $('#subscribe-btn').attr('disabled', true)
        }
        if (event.target.value === '' && $(this).hasClass('is-invalid')) {
            $(this).removeClass('is-invalid');
            $(this).offsetParent().removeClass('is-invalid');
        }
    } else if ($(this)[0]['validity']['valid'] === true && $(this).hasClass('is-invalid')) {
        $(this).removeClass('is-invalid');
        $(this).offsetParent().removeClass('is-invalid');
        $('#subscribe-btn').removeAttr('disabled')
    }
});

const svg_success = `
    <svg
        xmlns="http://www.w3.org/2000/svg"
        height="1em"
        viewBox="0 0 512 512"
        fill="currentColor"
    >
        <path
            d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM369 209L241 337c-9.4 9.4-24.6 9.4-33.9 0l-64-64c-9.4-9.4-9.4-24.6 0-33.9s24.6-9.4 33.9 0l47 47L335 175c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9z"
        />
    </svg>
`
const svg_error = `
    <svg
        xmlns="http://www.w3.org/2000/svg"
        height="1em"
        viewBox="0 0 512 512"
        fill="currentColor"
    >
        <path
            d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zm0-384c13.3 0 24 10.7 24 24V264c0 13.3-10.7 24-24 24s-24-10.7-24-24V152c0-13.3 10.7-24 24-24zM224 352a32 32 0 1 1 64 0 32 32 0 1 1 -64 0z"
        />
    </svg>
`
const svg_warning = `
    <svg
        xmlns="http://www.w3.org/2000/svg"
        height="1em"
        viewBox="0 0 512 512"
        fill="currentColor"
    >
        <path
            d="M256 32c14.2 0 27.3 7.5 34.5 19.8l216 368c7.3 12.4 7.3 27.7 .2 40.1S486.3 480 472 480H40c-14.3 0-27.6-7.7-34.7-20.1s-7-27.8 .2-40.1l216-368C228.7 39.5 241.8 32 256 32zm0 128c-13.3 0-24 10.7-24 24V296c0 13.3 10.7 24 24 24s24-10.7 24-24V184c0-13.3-10.7-24-24-24zm32 224a32 32 0 1 0 -64 0 32 32 0 1 0 64 0z"
        />
    </svg>
`
const subscribe_header_success = 'We are incredibly happy'
const subscribe_header_error = 'An error has occurred'
const subscribe_header_warning = 'Thank for your interest'

const subscribe_data_success = 'Thank you for subscribing to our newsletter. We\'ll soon start sending you the latest news, unique offers, events and discounts. Check your email as often as possible to stay up to date with all our company\'s offers'
const subscribe_data_error = 'You probably made a mistake and entered an incorrect email. Check the email and try again'
const subscribe_data_warning = 'We have detected that you have subscribed to our newsletter. If you do not receive our messages, please contact support'

function newsletterSubsctibe()
{
    let header = body = ''

    // $.ajax({
    //     url: url + 'newsletter',
    //     method: 'POST',
    //     dataType: 'json',
    //     username: username,
    //     password: password,
    //     data: {'email': $('#subscribeInput').val()},
    //     beforeSend: function() {
    //         $("#loader").show();
    //     },
    //     success: function(data){
    //         $("#loader").hide();
    //         console.log(data);
    //     }
    // });
    
// OK
    header = `
    <div class="border-start border-5 border-primary ps-3 fw-bold text-uppercase text-primary user-select-none">
        <div class="d-flex align-items-center font-roboto">
            <div>
                ` + svg_success + `
            </div>
            <div class="ps-1 font-roboto text-break">
                ` + subscribe_header_success + `
            </div>
        </div>
    </div>
    `
    body = subscribe_data_success

    header = `
    <div class="border-start border-5 border-primary ps-3 fw-bold text-uppercase text-primary user-select-none">
        <div class="d-flex align-items-center font-roboto">
            <div>
                ` + svg_error + `
            </div>
            <div class="ps-1 font-roboto text-break">
                ` + subscribe_header_error + `
            </div>
        </div>
    </div>
    `
    body = subscribe_data_error

    header = `
    <div class="border-start border-5 border-primary ps-3 fw-bold text-uppercase text-primary user-select-none">
        <div class="d-flex align-items-center font-roboto">
        <div>
            ` + svg_warning + `
        </div>
        <div class="ps-1 font-roboto text-break">
            ` + subscribe_header_warning + `
        </div>
        </div>
    </div>
    `
    body = subscribe_data_warning

    $('#newsletterSubscribeModalLabel').html(header)
    $('#newsletterSubscribeModalBody').html(body)
    $('#newsletterSubscribeModal').modal('show')
}
