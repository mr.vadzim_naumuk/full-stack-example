<?php
$call_rq = $error = '';
$infobar = [];

if (!empty($_REQUEST)) {
    if (!empty($_REQUEST['departure']) && !empty($_REQUEST['arrival']) && !empty($_REQUEST['date'])) {
        $tdepart = count($_REQUEST['departure']);
        $tarriv = count($_REQUEST['arrival']);
        $tdate = count($_REQUEST['date']);
        if ($tdepart == $tarriv && $tdepart == $tdate) {
            $index = 1;
            foreach($_REQUEST['departure'] as $key => $value) {
                $infobar[$key] = [
                    'from' => $_REQUEST['departure'][$key],
                    'to' => $_REQUEST['arrival'][$key],
                    'date' => $_REQUEST['date'][$key]
                ];
                $depart = (($index > 1) ? '&' : '')."T{$index}=".explode(' ', $value)[0];
                $index++;
                $arrival = "&T{$index}=".explode(' ', $_REQUEST['arrival'][$key])[0];
                $outdate = '&outdate'.($key+1).'='.date('Ymd', strtotime($_REQUEST['date'][$key]));
                $call_rq .= $depart.$arrival.$outdate;
                $index++;
                unset($depart, $arrival, $outdate);
            }
            unset($index);
            $call_rq .= '&D1=1&direct=0&cabin=Y&trip=one&lang=en';
        } else {
            header('Location: /');
        }
    } else {
        header('Location: /');
    }
}

self::set_vars('call_rq', $call_rq);
self::set_vars('info_bar', $infobar);
